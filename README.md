# SAM Java Lambda Function with API Gateway

This project demonstrates how to deploy a Java-based AWS Lambda function with API Gateway using AWS Serverless Application Model (SAM).

## Prerequisites

- Java Development Kit (JDK) 21
- Apache Maven
- AWS CLI
- AWS SAM CLI

## Project Structure

```
sam-java/
├── src/
│ └── main/
│ └── java/
│ └── com/
│ └── company/
│ └── lambda
│     └── functions
├── target/
├── .gitignore
├── pom.xml
├── README.md
└── template.yaml
```

## Deploy

```
mvn clean package
```

```
sam package --output-template-file packaged.yaml --s3-bucket YOUR_S3_BUCKET_NAME
```

Example Deploy Dev
```
sam deploy --template-file packaged.yaml --stack-name dev-users-management --capabilities CAPABILITY_IAM --parameter-overrides StageName=dev
```

Example Deploy SIT
```
sam deploy --template-file packaged.yaml --stack-name sit-users-management --capabilities CAPABILITY_IAM --parameter-overrides StageName=sit
```

