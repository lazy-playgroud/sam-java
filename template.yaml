AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'
Description: >
  Java Lambda Function with API Gateway and CloudWatch

Metadata:
  DeployVersion: '1.0.0'

Globals:
  Function:
    Timeout: 30
    MemorySize: 512
    Runtime: java21
    CodeUri: target/lambda-java-1.0-SNAPSHOT.jar
    Architectures: 
      - arm64
    Tags:
      PROJECT: poc-sam

Parameters:
  StageName:
    Type: String
    Default: "dev"
    Description: The stage name for the API Gateway deployment
    AllowedValues:
      - dev
      - sit
      - uat
      - pre-prod
      - prod
  EndpointA:
    Type: String
  ClientA:
    Type: String
  SecretA:
    Type: String    
Resources:
  ApiDeployment:
    Type: AWS::Serverless::Api
    Properties:
      StageName: !Ref StageName
      Description: !Sub 'API for ${StageName} stage'
      EndpointConfiguration:
        Type: REGIONAL
      AlwaysDeploy: true
      Tags:
        PROJECT: poc-sam

  LeadCreateFunction:
    Type: 'AWS::Serverless::Function'
    Properties:
      FunctionName: !Sub '${StageName}-lead-create'
      Handler: com.lazydev.lambda.leads.LeadCreateHandler::handleRequest
      MemorySize: 1024
      Policies:
        - AWSLambdaBasicExecutionRole
        - Statement:
            Effect: Allow
            Action: sns:Publish
            Resource: arn:aws:sns:${AWS::Region}:${AWS::AccountId}:dev-lead-management.fifo    
      Environment:
        Variables:
          SNS_TOPIC_ARN: !Sub 'arn:aws:sns:${AWS::Region}:${AWS::AccountId}:${StageName}-lead-management.fifo'
          ENDPOINT_A: ${EndpointA}
          CLIENT_A: ${ClientA} 
          SECRET_A: ${SecretA}
      Events:
        LeadCreateApi:
          Type: Api
          Properties:
            RestApiId: !Ref ApiDeployment
            Path: /lead
            Method: post
      AutoPublishAlias: live
      DeploymentPreference:
        Type: AllAtOnce # Or Canary10Percent5Minutes, Linear10PercentEvery1Minute, ...
      #SnapStart:
        #ApplyOn: PublishedVersions
      Tags:
        PROJECT: poc-sam

  LeadCreateWorkerFunction:
    Type: 'AWS::Serverless::Function'
    Properties:
      FunctionName: !Sub '${StageName}-lead-create-worker'
      Handler: com.lazydev.lambda.leads.workers.LeadCreateHandler::handleRequest
      Environment:
        Variables:
          SNS_TOPIC_ARN: !Sub 'arn:aws:sns:${AWS::Region}:${AWS::AccountId}:${StageName}-lead-management.fifo'
      Policies:
        - AWSLambdaBasicExecutionRole
        - Statement:
            Effect: Allow
            Action: sns:Publish
            Resource: arn:aws:sns:${AWS::Region}:${AWS::AccountId}:dev-lead-management.fifo
      Events:
        LeadCreateSQSTrigger:
          Type: SQS
          Properties:
            Queue: !Sub arn:aws:sqs:${AWS::Region}:${AWS::AccountId}:${StageName}-lead-create.fifo
      Tags:
        PROJECT: poc-sam

  DLQLeadCreateWorkerFunction:
    Type: 'AWS::Serverless::Function'
    Properties:
      FunctionName: !Sub '${StageName}-dlq-lead-create-worker'
      Handler: com.lazydev.lambda.leads.workers.DLQLeadCreateHandler::handleRequest
      Environment:
        Variables:
          TABLE_NAME: !Sub '${StageName}-lead-running-number'
      Policies:
        - AWSLambdaBasicExecutionRole
      Events:
        LeadCreateSQSTrigger:
          Type: SQS
          Properties:          
            Queue: !Sub arn:aws:sqs:${AWS::Region}:${AWS::AccountId}:${StageName}-dlq-lead-create.fifo
      Tags:
        PROJECT: poc-sam
         
  LeadSyncWorkerFunction:
    Type: 'AWS::Serverless::Function'
    Properties:
      FunctionName: !Sub '${StageName}-lead-sync-worker'
      Handler: com.lazydev.lambda.leads.workers.LeadSyncHandler::handleRequest
      Policies:
        - AWSLambdaBasicExecutionRole
        - DynamoDBCrudPolicy:
            TableName: !Sub '${AWS::AccountId}::${AWS::Region}-${StageName}-lead-running-number'
      Events:
        LeadSyncSQSTrigger:
          Type: SQS
          Properties:
            Queue: !Sub 'arn:aws:sqs:${AWS::Region}:${AWS::AccountId}:${StageName}-lead-sync.fifo'
      Tags:
        PROJECT: poc-sam

  DLQLeadSyncWorkerFunction:
    Type: 'AWS::Serverless::Function'
    Properties:
      FunctionName: !Sub '${StageName}-dlq-lead-sync-worker'
      Handler: com.lazydev.lambda.leads.workers.DLQLeadSyncHandler::handleRequest
      Policies:
        - AWSLambdaBasicExecutionRole
        - DynamoDBCrudPolicy:
            TableName: !Sub '${AWS::AccountId}::${AWS::Region}-${StageName}-lead-running-number'
      Events:
        LeadSyncSQSTrigger:
          Type: SQS
          Properties:
            Queue: !Sub 'arn:aws:sqs:${AWS::Region}:${AWS::AccountId}:${StageName}-dlq-lead-sync.fifo'
      Tags:
        PROJECT: poc-sam
