package com.lazydev.lambda.leads.workers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;

public class LeadSyncHandler implements RequestHandler<SQSEvent, Void> {

    @Override
    public Void handleRequest(SQSEvent event, Context context) {
        // Log the entire event
        context.getLogger().log("Received event: " + event.toString());
        for (SQSEvent.SQSMessage message : event.getRecords()) {
            // Process each message
            System.out.println("Message Body: " + message.getBody());
            // Your business logic here
        }
        return null;
    }
}