package com.lazydev.lambda.leads.workers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishResponse;
import com.lazydev.lambda.utils.SNSPublisher;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LeadCreateHandler implements RequestHandler<SQSEvent, Void> {

    private static final String SNS_TOPIC_ARN = "SNS_TOPIC_ARN";
    private static final String SUBJECT = "LEAD_SYNC";
    private static final String MESSAGE_GROUP_ID = "MESSAGE_GROUP_ID";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public Void handleRequest(SQSEvent event, Context context) {

        String topicArn = System.getenv(SNS_TOPIC_ARN);
        if (topicArn == null || topicArn.isEmpty()) {
            context.getLogger().log("SNS_TOPIC_ARN environment variable is not set");
            return null;
        }

        for (SQSEvent.SQSMessage message : event.getRecords()) {
            processMessage(message, context, topicArn);
        }

        return null;
    }

    private void processMessage(SQSEvent.SQSMessage message, Context context, String topicArn) {
        String messageBody = message.getBody();
        context.getLogger().log("Message Body: " + messageBody);

        try {
            String originalMessage = extractMessage(messageBody, context);
            if (originalMessage != null) {
                // Insert into RDS
                insertIntoDatabase(originalMessage, context);

                // Publish to SNS
                publishToSNS(originalMessage, context, topicArn);
            }
        } catch (Exception e) {
            context.getLogger().log("Error processing message: " + e.getMessage());
        }
    }

    private String extractMessage(String messageBody, Context context) throws Exception {
        JsonNode rootNode = OBJECT_MAPPER.readTree(messageBody);
        JsonNode messageNode = rootNode.get("Message");

        if (messageNode == null) {
            context.getLogger().log("Message field is missing in the SQS message");
            return null;
        }

        String originalMessage = messageNode.asText();
        context.getLogger().log("Original Message: " + originalMessage);
        return originalMessage;
    }

    private void publishToSNS(String message, Context context, String topicArn) {
        try (SNSPublisher snsPublisher = new SNSPublisher(topicArn)) {
            String messageDeduplicationId = UUID.randomUUID().toString();
            context.getLogger().log("messageDeduplicationId: " + messageDeduplicationId);

            Map<String, MessageAttributeValue> attributes = new HashMap<>();
            attributes.put("Subject", MessageAttributeValue.builder()
                    .dataType("String")
                    .stringValue(SUBJECT)
                    .build());

            PublishResponse publishResponse = snsPublisher.publishMessage(
                    message,
                    SUBJECT,
                    MESSAGE_GROUP_ID,
                    messageDeduplicationId,
                    attributes
            );

            context.getLogger().log("PublishResponse: " + publishResponse.toString());
        } catch (Exception e) {
            context.getLogger().log("Error publishing to SNS: " + e.getMessage());
        }
    }

    private void insertIntoDatabase(String message, Context context) {
        String jdbcUrl = System.getenv("JDBC_URL");
        String username = System.getenv("DB_USERNAME");
        String password = System.getenv("DB_PASSWORD");
        context.getLogger().log("Inserting database....");

        if (jdbcUrl == null || username == null || password == null) {
            context.getLogger().log("Database connection information is not set");
            return;
        }
        
    }
}
