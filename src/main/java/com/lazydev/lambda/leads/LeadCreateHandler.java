package com.lazydev.lambda.leads;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishResponse;
import com.lazydev.lambda.utils.SNSPublisher;
import com.lazydev.lambda.utils.HandleHttpResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LeadCreateHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final String SNS_TOPIC_ARN = "SNS_TOPIC_ARN";
    private static final String SUBJECT = "LEAD_CREATE";
    private static final String MESSAGE_GROUP_ID = "MESSAGE_GROUP_ID";
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent input, Context context) {
        context.getLogger().log("Received event: " + input.toString());

        String topicArn = System.getenv(SNS_TOPIC_ARN);
        if (topicArn == null || topicArn.isEmpty()) {
            context.getLogger().log("SNS_TOPIC_ARN environment variable is not set");
            return HandleHttpResponse.createErrorResponse(500, "SNS_TOPIC_ARN environment variable is not set");
        }

        try (SNSPublisher snsPublisher = new SNSPublisher(topicArn)) {
            String message = input.getBody();
            String uuid = UUID.randomUUID().toString();
            context.getLogger().log("Generated UUID: " + uuid);

            String messageWithUuid = mergeUuidWithMessage(message, uuid);
            context.getLogger().log("Message with UUID: " + messageWithUuid);

            String messageDeduplicationId = input.getRequestContext().getRequestId();
            context.getLogger().log("messageDeduplicationId: " + messageDeduplicationId);

            Map<String, MessageAttributeValue> attributes = new HashMap<>();
            attributes.put("Subject", MessageAttributeValue.builder()
                    .dataType("String")
                    .stringValue(SUBJECT)
                    .build());

            PublishResponse publishResponse = snsPublisher.publishMessage(
                    messageWithUuid,
                    SUBJECT,
                    MESSAGE_GROUP_ID,
                    messageDeduplicationId,
                    attributes
            );

            context.getLogger().log("PublishResponse: " + publishResponse.toString());

            Map<String, Object> responseBodyMap = new HashMap<>();
            responseBodyMap.put("message", "Message sent to SNS successfully");
            responseBodyMap.put("responseBody", Map.of(
                    "messageId", publishResponse.messageId(),
                    "uuid", uuid
            ));

            return HandleHttpResponse.createSuccessResponse(200, responseBodyMap);
        } catch (Exception e) {
            context.getLogger().log("Error: " + e.getMessage());
            return HandleHttpResponse.createErrorResponse(500, "Internal Server Error");
        }
    }

    private String mergeUuidWithMessage(String message, String uuid) throws Exception {
        Map<String, Object> messageMap = objectMapper.readValue(message, HashMap.class);
        messageMap.put("uuid", uuid);
        return objectMapper.writeValueAsString(messageMap);
    }
}
