package com.lazydev.lambda.utils;

import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

import java.util.Map;

public class SNSPublisher implements AutoCloseable {
    private final SnsClient snsClient;
    private final String topicArn;

    public SNSPublisher(String topicArn) {
        this.snsClient = SnsClient.builder().build();
        this.topicArn = topicArn;
    }

    public PublishResponse publishMessage(String message, String subject, String messageGroupId, String messageDeduplicationId, Map<String, MessageAttributeValue> attributes) {
        PublishRequest publishRequest = PublishRequest.builder()
                .topicArn(topicArn)
                .subject(subject)
                .messageGroupId(messageGroupId)
                .messageDeduplicationId(messageDeduplicationId)
                .message(message)
                .messageAttributes(attributes)
                .build();

        return snsClient.publish(publishRequest);
    }

    @Override
    public void close() {
        snsClient.close();
    }
}
