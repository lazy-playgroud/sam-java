package com.lazydev.lambda.utils;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class HandleHttpResponse {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static APIGatewayProxyResponseEvent createSuccessResponse(int statusCode, Map<String, Object> responseBodyMap) {
        return createResponse(statusCode, responseBodyMap);
    }

    public static APIGatewayProxyResponseEvent createErrorResponse(int statusCode, String errorMessage) {
        Map<String, String> errorResponseMap = Map.of("error", errorMessage);
        return createResponse(statusCode, errorResponseMap);
    }

    private static APIGatewayProxyResponseEvent createResponse(int statusCode, Map<String, ?> responseBodyMap) {
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
        try {
            String responseBody = objectMapper.writeValueAsString(responseBodyMap);
            response.setStatusCode(statusCode);
            response.setBody(responseBody);
        } catch (Exception e) {
            response.setStatusCode(500);
            response.setBody("{\"error\": \"Error serializing response\"}");
        }
        return response;
    }
}
