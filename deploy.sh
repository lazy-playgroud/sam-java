set -x
echo "Script is running"

ENV="dev"
sam deploy \
  --stack-name my-app-${ENV} \
  --parameter-overrides file://environments/${ENV}/parameters.json \
  --capabilities CAPABILITY_IAM
